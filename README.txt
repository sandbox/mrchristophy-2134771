A super-simple module to help you obtain the latest node without using views.

USAGE:

$latest = get_latest('article');

If you want to include unpublished results:

$latest = get_latest('article', FALSE);
